import eslintPlugin from "vite-plugin-eslint";
export default defineNuxtConfig({
  ssr: false,
  css: ["vuetify/lib/styles/main.sass", "@mdi/font/css/materialdesignicons.css"],
  build: {
    transpile: ["vuetify"]
  },
  sourcemap: {
    server: true,
    client: true
  },
  typescript: {
    // strict: true,
    typeCheck: true
  },
  vite: {
    plugins: [
      eslintPlugin({ fix: true })
    ]
  },
});
