
interface Feature {
  name: string
  icon?: string
  description: string
}

interface Treatment extends Feature {
  id: number
  duration: number
}

interface Doctor {
  id: number
  name: string
  specialization: string
  treatments: number[]
  image: string
}
