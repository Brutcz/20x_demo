export const useData = () => {
  const treatments = ref<Treatment[]>([
    { id: 1, name: "Treatment name 01", duration: 30, icon: "mdi-baby-carriage", description: "Regular treatment. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec iaculis gravida nulla. In dapibus augue non sapien. Nulla est. Phasellus rhoncus. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Morbi leo mi, nonummy eget tristique non, rhoncus non leo." },
    { id: 2, name: "Treatment name 02", duration: 45, icon: "mdi-candy-outline", description: "Longer treatment. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec iaculis gravida nulla. In dapibus augue non sapien. Nulla est. Phasellus rhoncus. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Morbi leo mi, nonummy eget tristique non, rhoncus non leo." },
    { id: 3, name: "Treatment name 03", duration: 25, icon: "mdi-battery-charging-100", description: "Very short treatment. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec iaculis gravida nulla. In dapibus augue non sapien. Nulla est. Phasellus rhoncus. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Morbi leo mi, nonummy eget tristique non, rhoncus non leo." },
    { id: 4, name: "Treatment name 04", duration: 60, icon: "mdi-alarm-light-outline", description: "More longer treatment. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec iaculis gravida nulla. In dapibus augue non sapien. Nulla est. Phasellus rhoncus. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Morbi leo mi, nonummy eget tristique non, rhoncus non leo." },
    { id: 5, name: "Treatment name 05", duration: 20, icon: "mdi-account-clock-outline", description: "Shorter treatment. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec iaculis gravida nulla. In dapibus augue non sapien. Nulla est. Phasellus rhoncus. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Morbi leo mi, nonummy eget tristique non, rhoncus non leo." },
    { id: 6, name: "Treatment name 06", duration: 120, icon: "mdi-hospital-building", description: "Very long treatment. Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec iaculis gravida nulla. In dapibus augue non sapien. Nulla est. Phasellus rhoncus. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Morbi leo mi, nonummy eget tristique non, rhoncus non leo." }
  ]);

  const features = ref<Feature[]>([
    { name: "Feature name 01", icon: "mdi-bacteria-outline", description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec iaculis gravida nulla. In dapibus augue non sapien. Nulla est. Phasellus rhoncus. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Morbi leo mi, nonummy eget tristique non, rhoncus non leo." },
    { name: "Feature name 02", icon: "mdi-beaker-outline", description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec iaculis gravida nulla. In dapibus augue non sapien. Nulla est. Phasellus rhoncus. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Morbi leo mi, nonummy eget tristique non, rhoncus non leo." },
    { name: "Feature name 03", icon: "mdi-bike", description: "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec iaculis gravida nulla. In dapibus augue non sapien. Nulla est. Phasellus rhoncus. Sed vel lectus. Donec odio tempus molestie, porttitor ut, iaculis quis, sem. Morbi leo mi, nonummy eget tristique non, rhoncus non leo." }
  ]);

  const doctors = ref<Doctor[]>([
    { id: 1, name: "Doctor 01", specialization: "special 1", image: "https://randomuser.me/api/portraits/women/81.jpg", treatments: [1, 2, 3] },
    { id: 2, name: "Doctor 02", specialization: "special 2", image: "https://randomuser.me/api/portraits/men/82.jpg", treatments: [1, 4, 5] },
    { id: 3, name: "Doctor 03", specialization: "special 3", image: "https://randomuser.me/api/portraits/men/83.jpg", treatments: [1, 2, 3] },
    { id: 4, name: "Doctor 04", specialization: "special 4", image: "https://randomuser.me/api/portraits/women/84.jpg", treatments: [1, 3, 5] },
    { id: 5, name: "Doctor 05", specialization: "special 5", image: "https://randomuser.me/api/portraits/men/85.jpg", treatments: [1, 2, 4] },
    { id: 6, name: "Doctor 06", specialization: "special 6", image: "https://randomuser.me/api/portraits/women/86.jpg", treatments: [1, 2, 3, 4, 5] },
    { id: 7, name: "Doctor 07", specialization: "special 7", image: "https://randomuser.me/api/portraits/women/87.jpg", treatments: [1, 2, 3] }
  ]);

  return {
    treatments,
    features,
    doctors
  };
};
